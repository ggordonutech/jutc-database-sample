package lab6.inventory.viewmodels;

import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

import lab6.inventory.models.Marker;

public class MarkerVM extends Marker {

	private static final long serialVersionUID = 1L;
	private JCheckBox chkInUse;

	public MarkerVM() {
		this(0, "");
	}

	public MarkerVM(int id, String name) {
		super(id, name);
		chkInUse = new JCheckBox();
		chkInUse.setToolTipText("Select " + name);
		chkInUse.setName(id + "");
	}

	public MarkerVM(Marker m) {
		this(m.getId(), m.getName());
	}

	public boolean isSelected() {
		return chkInUse.isSelected();
	}

	public void addActionListener(ActionListener al) {
		chkInUse.addActionListener(al);
	}

	public JCheckBox getCheckBoxInUse() {
		return chkInUse;
	}

	public Object[] toObjectArray() {
		return new Object[] { getId(), getName(), "Marker",
				new Boolean(chkInUse.isSelected()) };
	}

	public void setValueAtIndex(int index, Object value) {
		switch (index) {
		case 0: // this is the id - do nothing
			break;
		case 1:
			setName((String) value);
			break;
		case 2: // this is the type - do nothing
			break;
		case 3:
			chkInUse.setSelected((Boolean) value);
			break;
		}
	}

}
