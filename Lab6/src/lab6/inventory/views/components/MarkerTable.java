package lab6.inventory.views.components;

import java.util.ArrayList;
import java.util.List;




import javax.swing.JTable;



import lab6.inventory.models.Marker;
import lab6.inventory.viewmodels.MarkerVM;

public class MarkerTable extends JTable {

	private static final long serialVersionUID = 1L;

	public MarkerTable() {
		this(null);
	}

	public MarkerTable(List<Marker> markers){
		super(new MarkerTableModel(markers == null ? new ArrayList<Marker>() : markers));
	
		initTable(markers);
	}
	
	private void initTable(List<Marker> markers){
		//Rename columns
	    for(int i=0;i<MarkerTableModel.COLUMN_NAMES.length;i++){
	    	getColumnModel().getColumn(i).setHeaderValue(MarkerTableModel.COLUMN_NAMES[i]);
	    }
		
	}
	
	public void refreshData(List<Marker> markers){
		if(markers == null){
			markers = new ArrayList<Marker>();
		}
		System.out.println("Refreshing "+markers.size());
		this.dataModel = new MarkerTableModel(markers);
		
		this.repaint();
	}
	
	public List<MarkerVM>  getSelectedMarkers(){
		return ((MarkerTableModel)this.dataModel).getSelectedMarkers();
		
	}
	
	public boolean isAnySelected(){
		return ((MarkerTableModel)this.dataModel).isAnySelected();
	}
	
	

}
